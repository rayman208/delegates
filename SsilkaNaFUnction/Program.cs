﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Birja
{
    class Program
    {
        static void Main(string[] args)
        {
            //MyEvent me = new MyEvent();
            //me.hasEvent = false;

            //Broker b = new Broker(me);
            //PhysicalFace pf = new PhysicalFace(me);
            //YurFace yf = new YurFace(me);

            //Task.Run(() => pf.Watch());
            //Task.Run(() => yf.Watch());

            //b.Info();

            Broker b = new Broker();
            PhysicalFace pf = new PhysicalFace();
            YurFace yf = new YurFace();

            b.OnInfo += pf.Buy;
            b.OnInfo += yf.Buy;
            b.Info(5);

            b.OnInfo -= yf.Buy;
            b.Info(20);


            Console.ReadKey();

        }
    }
}
